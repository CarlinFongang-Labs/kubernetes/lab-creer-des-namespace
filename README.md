# Création et gestion de Namespace sous Kubernetes

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Contexte | Scenario

Vous travaillez pour ./BeeHive, une société de services d'abonnement qui propose des expéditions hebdomadaires d'abeilles aux clients. L'entreprise est en train de conteneuriser son infrastructure et d'exécuter ses logiciels sur Kubernetes. Dans le cadre de ce processus, l'entreprise travaille à déterminer de quels espaces de noms elle aura besoin dans le cluster Kubernetes.

Il vous a été demandé d'accéder au cluster et d'effectuer certaines tâches de maintenance liées aux espaces de noms du cluster.

- L'équipe de développement souhaite pouvoir travailler dans un espace de noms distinct des espaces de noms utilisés pour exécuter les charges de travail de production. Créez un nouvel espace de noms appelé dev.

- L'un des membres de votre équipe de sécurité souhaite auditer les espaces de noms qui existent actuellement dans le cluster. 

- Obtenez une liste des espaces de noms actuels et enregistrez-la dans un fichier situé /home/cloud_user/namespaces.txtsur le nœud du plan de contrôle.

- Un membre de l'équipe a créé un pod avec le nom quark, mais il ne sait pas dans quel espace de noms il se trouve. Déterminez dans quel espace de noms se trouve ce pod et enregistrez le nom de cet espace de noms dans un fichier situé sur `/home/cloud_user/quark-namespace.txt`, le nœud du du control plan.

# Objectif

1. Créer l'espace de noms `dev`

2. Obtenir une liste des espaces de noms actuels

3. Trouver l'espace de noms du pod « quark »

# Application

## 1. Connexion au **Control Plane**
Le control plan est tout simplement l'instance qui permet de piloter le cluster kubernetes que nous avons déployé dans un précédent Labs.

Pour nous connecter au controle plan en ssh, nous allons simplement passer la commande suivante dans notre terminal

````bash
ssh -i id_rsa user@public_ip_address
````

celà correspond dans mon cas à 

````bash
ssh -i devops-aCD.pem cloud_user@3.236.164.56
````

devops-aCD étant la clé SSH privée générée pour l'authentification sans mot de passe utilisateur sur mon serveur distant.

## 2. Création d'un namespace et divers opérations

Une fois connecté à l'instance **Control Plan**, nous allons passer les commandes suivantes :

### 1. Création du namespace **"dev"**

````bash
kubectl create namespace dev
````

>![Alt text](img/image.png)
*Namespace "dev" est bien crée*

### 2. Lister les **namespace**

nous pouvons lister ces namespace à l'aide de la commande

````bash
kubectl get namespace
````

>![Alt text](img/image-1.png)
*Liste de namespace disponible sur le Control Plan*

### 3. Sauvegade de la liste de namespace dans un fichier texte

````bash
kubectl get namespace > /home/$USER/namespaces.txt
````
>![Alt text](img/image-2.png)
*Création du fichier texte*

### 4. Vérification de la liste de namespace dans le fichier texte

````bash
cat namespaces.txt 
````

>![Alt text](img/image-3.png)
*Liste de namespace sauvegardée*

## 3. Retrouver un pod spécifique dans un ensemble de namepsace dans un cluster k8s

### 1. Recherche du pod **"quark"**

en entrant simplement la commande `kubectl get pod` sur notre cluster, nous n'observons aucun resultat, ce qui n'est pas très satisfaisant.

pour rémédier à cette difficulter et retrouver les pods et notamment le pod qui nous est demandé, nous allons entrer les commandes suivantes : 

````bash
kubectl get pods --all-namespaces
````

Cela nous permet de lister tout les pods dans l'ensemble des namespaces disponible sur le Control Plan

>![Alt text](img/image-4.png)
*Le pod "quark" recherché se trouve ainsi à la fin de la liste*


### 2. Sauvegard des reférences du pod **"quark"** dans un fichier texte


````bash
echo oasis > /home/$USER/quark-namespace.txt
````

>![Alt text](img/image-5.png)
*Sauvegarde du namesapce où se trouve le pod "quark"*